package tl_noc_testbench;
  
  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
  import SpecialFIFOs :: * ;
  import LFSR::*;
  import Vector::*;
  import Connectable::*;

  // Fabric TileLink Imports
  import Tilelink_Types::*;
  import address_map::*;
  import tl_master::*;
  import tl_slave::*;
  import Tilelink_NIC::*;

  // Shakti - Open Smart Imports 
  import Types::*;
  import MessageTypes::*;
  import VirtualChannelTypes::*;
  import RoutingTypes::*;
  import CreditTypes::*;

  import Network::*;
  import CreditUnit::*;
  import StatLogger::*;
  import MonitorStats::*;


  `define NUM_Masters 16
  `define NUM_Slaves 16
  `define StopInjection 100000
  `define EndSim 105000
  // module mk_dummy_network(NetworkOuterInterface);
    
  // endmodule

  (*synthesize*)
  module mk_tilelink_noc_tg_testbench(Empty);
    
    Reg#(Bit#(32)) rg_cycle <- mkReg(0);
    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) inited    <- mkReg(False);
    Reg#(Data) initCount <- mkReg(0);
    
    // Random num gen 
    LFSR#(Bit#(32)) rand_txn_signature <- mkLFSR_32();
    Wire#(Bit#(32)) wr_rand_value <- mkWire();

    // Traffic Generators
    Vector#(MeshHeight,Vector#(MeshWidth,Ifc_tl_master_tg#(32, 8,2,4,4))) v_master_tg = newVector;
    Vector#(MeshHeight,Vector#(MeshWidth,Ifc_tl_slave_tg#(32,8,2,4,4))) v_slave_tg = newVector;
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
    for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
      // Coordinate to Master slave id
      Integer id = (valueOf(MeshWidth)*i)+j; // { i :: x , j :: y }
      v_master_tg[i][j] <- mk_tl_master_tg(fromInteger(id),TL_C);
      v_slave_tg[i][j]  <- mk_tl_slave_tg (fromInteger(id),TL_C);
      end
    end

    // // Tile Link Network on Chip  Fabric
    Network meshNtk <- mkNetwork;

    // Noc -Tile Link Transactors
    Vector#(MeshHeight,Vector#(MeshWidth,TL_NoC_Endpoint#(32,8,2,4,4))) v_node_tilelink_endpoint = newVector;

    // Simulation Performance Monitoring Module
    MonitorStats monitorStats <- mkMonitorStats;
    mkConnection(monitorStats.monitor_ingress, meshNtk.monitor_node_ingress);
    mkConnection(monitorStats.monitor_egress, meshNtk.monitor_node_egress);

    // House Keeping
    rule init(!inited);
      initCount <= initCount + 1;
      if(meshNtk.isInited && initCount > fromInteger(valueOf(MeshHeight)) + fromInteger(valueOf(MeshWidth)))  begin
        inited <= True;
      end
    endrule
    rule rl_initial(inited && !rg_start);
      rg_start <= True;
      rand_txn_signature.seed(32'hBEBECAFE);
    endrule
    rule rl_sim_time;
      rg_cycle <= rg_cycle + 1;
    endrule
    
    rule rl_next_txn(rg_start==True);
      wr_rand_value <= rand_txn_signature.value();
      rand_txn_signature.next();
    endrule
    
    rule finishBench(inited && (rg_cycle == `EndSim));
      // print the STATS.
      // TODO : vnet, criticality, and tilelink specific stats can be added here.
      let hi_total_lat = meshNtk.latencyStats[0].getLatencyCount;
      let lo_total_lat = meshNtk.latencyStats[1].getLatencyCount; 
      let hi_max = meshNtk.latencyStats[0].maxLatency;
      let lo_max = meshNtk.latencyStats[1].maxLatency;
      $display("Total LO latency is: %d", lo_total_lat);
      $display("Total HI latency is: %d", hi_total_lat);
      $display("\n Maximum LO latency is: %d", lo_max);
      $display("Maximum HI latency is: %d\n", hi_max);
      monitorStats.printFinalStats;
      $display("End ofSimulation !");
      $finish;
    endrule

    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        v_node_tilelink_endpoint[i][j] <- mkConnect_TL_NOC(meshNtk.ntkPorts[i][j],i,j);
      end
    end
    
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
        // NIC Configuration
        rule injection_rate_gov(inited);
          v_node_tilelink_endpoint[i][j].injection_rate_governor(True);
        endrule
        // Initialise Traffic Generators
        rule rl_start_m(rg_start == True);
        v_master_tg[i][j].ma_start();
        endrule
        rule rl_drive_random_m(rg_start == True );
          v_master_tg[i][j].ma_rand_value(wr_rand_value);
        endrule
        rule rl_finish_m(rg_cycle == `StopInjection);
          v_master_tg[i][j].ma_finish();
        endrule

        rule rl_start_s(rg_start == True);
          v_slave_tg[i][j].ma_start();
        endrule
        rule rl_drive_random_s(rg_start == True );
          v_slave_tg[i][j].ma_rand_value(wr_rand_value);
        endrule
        rule rl_finish_s(rg_cycle == `StopInjection);
          v_slave_tg[i][j].ma_finish();
        endrule
      end
    end
    for(Integer i=0; i<valueOf(MeshHeight); i=i+1) begin
      for(Integer j=0; j<valueOf(MeshWidth); j=j+1) begin
       
        mkConnection(v_node_tilelink_endpoint[i][j].node_master,v_slave_tg[i][j].tl_slave);
        mkConnection(v_node_tilelink_endpoint[i][j].node_slave ,v_master_tg[i][j].tl_master);
      end
    end
  endmodule  

endpackage : tl_noc_testbench