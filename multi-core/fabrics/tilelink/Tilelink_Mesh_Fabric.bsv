package Tilelink_Mesh_Fabric;

import Tilelink_Types::*;
`include "system.defines"
import GetPut ::*;
import Vector::*;
import FIFO ::*;
import FIFOF::*;
import SpecialFIFOs ::*;
import Semi_FIFOF::*;
import Connectable ::*;
interface Ifc_Tilelink_Fabric #(numeric type a,numeric type w,  numeric type z,
                                numeric type o,numeric type i ,numeric type num_masters , numeric type num_slaves);
  interface Vector #(num_masters, Ifc_tlc_slave#(a, w, z ,o,i))  v_master_port;
  interface Vector #(num_slaves,  Ifc_tlc_master#(a, w, z,o,i))  v_slave_port;
endinterface

function Vector#(num_masters,Vector#(num_slaves,Bool)) fn_route_all;
  return replicate(replicate(True));
endfunction

module mkTilelink_Mesh_Fabric#(
  function Bit#(i) address_map (Bit#(a) address),
  Vector#(num_masters,Vector#(num_slaves,Bool)) connectivity,
  TL_Capability capability)(Ifc_Tilelink_Fabric #(a,w,z,o,i,num_masters,num_slaves));

  // Making a vector of FIFOF`s for each master - slave
  Vector#(num_masters,Ifc_tlc_slave_agent#(a,w,z,o,i)) v_slave_agent   <- replicateM (mktlc_slave_agent);
  Vector#(num_slaves, Ifc_tlc_master_agent#(a,w,z,o,i))v_master_agent  <- replicateM (mktlc_master_agent);

  function Bool fn_guard_channel_a(Integer master_id,Integer slave_id);
    A_channel#(a,w,z,o) lv_packet_a = v_slave_agent[master_id].o_a_channel.first();
    Bit#(i) lv_slaveid = address_map(lv_packet_a.a_address);
    Bit#(i) lv_temp = fromInteger(slave_id);
    return (lv_slaveid == lv_temp);
  endfunction 
  function Bool fn_guard_channel_c(Integer master_id,Integer slave_id);
    C_channel#(a,w,z,o) lv_packet_c = v_slave_agent[master_id].o_c_channel.first();
    Bit#(i) lv_slaveid = address_map(lv_packet_c.c_address);
    Bit#(i) lv_temp = fromInteger(slave_id);
    return (lv_slaveid == lv_temp);
  endfunction 
  function Bool fn_guard_channel_e(Integer master_id,Integer slave_id);
    E_channel#(i)        lv_packet_e = v_slave_agent[master_id].o_e_channel.first();
    Bit#(i) lv_slaveid = lv_packet_e.e_sink;
    Bit#(i) lv_temp = fromInteger(slave_id);
    return (lv_slaveid == lv_temp);
  endfunction 
  function Bool fn_guard_channel_b(Integer master_id,Integer slave_id);
    B_channel#(a,w,z,o) lv_packet_b = v_master_agent[slave_id].o_b_channel.first();
    Bit#(o) lv_masterid = lv_packet_b.b_source;
    Bit#(o) lv_temp = fromInteger(master_id);
    return (lv_masterid == lv_temp);
  endfunction 
  function Bool fn_guard_channel_d(Integer master_id,Integer slave_id);
    D_channel#(w,z,o,i)   lv_packet_d = v_master_agent[slave_id].o_d_channel.first();
    Bit#(o) lv_masterid = lv_packet_d.d_source;
    Bit#(o) lv_temp = fromInteger(master_id);
    return (lv_masterid == lv_temp);
  endfunction 
  // Rule Sets Connecting The Master and Slave Channels
  // A-Channel Deq - Master Enq Slave
  Rules rls_channel_a = emptyRules();
  for(Integer m = 0; m < valueOf(num_masters); m = m+1) begin
    for(Integer s = 0; s < valueOf(num_slaves); s = s+1) begin
      Rules rls_connect_a = emptyRules();
      if(connectivity[m][s] == True)begin
        rls_connect_a = (rules
          rule rl_connect_channel_a(fn_guard_channel_a(m,s));
            v_master_agent[s].i_a_channel.enq(v_slave_agent[m].o_a_channel.first());
            v_slave_agent[m].o_a_channel.deq();
          endrule
        endrules);
        rls_channel_a = rJoinPreempts(rls_channel_a,rls_connect_a);// Lower indexed members haave higher priority
      end
    end
  end
  // C-Channel Deq - Master Enq Slave
  Rules rls_channel_c = emptyRules();
  for(Integer m = 0; m < valueOf(num_masters); m = m+1) begin
    for(Integer s = 0; s < valueOf(num_slaves); s = s+1) begin
      Rules rls_connect_c = emptyRules();
      if(connectivity[m][s] == True)begin
        rls_connect_c = (rules
          rule rl_connect_channel_c(fn_guard_channel_c(m,s));
            v_master_agent[s].i_c_channel.enq(v_slave_agent[m].o_c_channel.first());
            v_slave_agent[m].o_c_channel.deq();
          endrule
        endrules);
        rls_channel_c = rJoinPreempts(rls_channel_c,rls_connect_c);// Lower indexed members haave higher priority
      end
    end
  end
  // E-Channel Deq - Master Enq Slave
  Rules rls_channel_e = emptyRules();
  for(Integer m = 0; m < valueOf(num_masters); m = m+1) begin
    for(Integer s = 0; s < valueOf(num_slaves); s = s+1) begin
      Rules rls_connect_e = emptyRules();
      if(connectivity[m][s] == True)begin
        rls_connect_e = (rules
          rule rl_connect_channel_e(fn_guard_channel_e(m,s));
            v_master_agent[s].i_e_channel.enq(v_slave_agent[m].o_e_channel.first());
            v_slave_agent[m].o_e_channel.deq();
          endrule
        endrules);
        rls_channel_e = rJoinPreempts(rls_channel_e,rls_connect_e);// Lower indexed members haave higher priority
      end
    end
  end
  // B-Channel Deq - Slave  Enq Master
  Rules rls_channel_b = emptyRules();
  for(Integer s = 0; s < valueOf(num_slaves); s = s+1) begin
    for(Integer m = 0; m < valueOf(num_masters); m = m+1) begin
      Rules rls_connect_b = emptyRules();
      if(connectivity[m][s] == True)begin
        rls_connect_b = (rules
          rule rl_connect_channel_b(fn_guard_channel_b(m,s));
            v_slave_agent[m].i_b_channel.enq(v_master_agent[s].o_b_channel.first());
            v_master_agent[s].o_b_channel.deq();
          endrule
        endrules);
        rls_channel_b = rJoinPreempts(rls_channel_b,rls_connect_b);// Lower indexed members haave higher priority
      end
    end
  end
  // D-Channel Deq - Slave  Enq Master
  Rules rls_channel_d = emptyRules();
  for(Integer s = 0; s < valueOf(num_slaves); s = s+1) begin
    for(Integer m = 0; m < valueOf(num_masters); m = m+1) begin
      Rules rls_connect_d = emptyRules();
      if(connectivity[m][s] == True)begin
        rls_connect_d = (rules
          rule rl_connect_channel_d(fn_guard_channel_d(m,s));
            v_slave_agent[m].i_d_channel.enq(v_master_agent[s].o_d_channel.first());
            v_master_agent[s].o_d_channel.deq();
          endrule
        endrules);
        rls_channel_d = rJoinPreempts(rls_channel_d,rls_connect_d);// Lower indexed members haave higher priority
      end
    end
  end
  
  // Add Rule Sets based on "capability"
  if((capability == TL_UL)||(capability == TL_UH)||(capability == TL_C))begin
    addRules(rls_channel_a);
    addRules(rls_channel_d);
  end
  if(capability == TL_C)begin
    addRules(rls_channel_b);
    addRules(rls_channel_c);
    addRules(rls_channel_e);
  end

  // Creating External Interface Sets.

  function Ifc_tlc_slave#(a, w, z, o, i) gen_master_port (Integer index) = v_slave_agent[index].tilelink_side ;
  function Ifc_tlc_master#(a, w, z, o, i) gen_slave_port (Integer index) = v_master_agent[index].tilelink_side;
  interface v_master_port = genWith(gen_master_port);
  interface v_slave_port  = genWith(gen_slave_port);

endmodule

endpackage : Tilelink_Mesh_Fabric