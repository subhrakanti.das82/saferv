package tb_shakti_slave;

  import StmtFSM :: * ;
  import GetPut::*;
  import FIFO::*;
  import FIFOF::*;
  import SpecialFIFOs :: * ;
  import Semi_FIFOF::*;
  import LFSR::*;
  import ShaktiLink_Types::*;
  import Tilelink_Types::*;


  interface Ifc_sl_slave_tg#(numeric type a, numeric type w, numeric type o,
  numeric type i, numeric type op, numeric type acks, numeric type u);
    method Action ma_start;
    method Action ma_finish;
    method Action ma_rand_value(Bit#(a) lfsr_value);
    interface Ifc_slc_slave#(a,w,o,i,op,acks,u) sl_slave;
    method Bool mv_done;
  endinterface

  (*synthesize*)
  module mk_instance_sl_slave_tg(Empty);
    Ifc_sl_slave_tg#(32, 64,2,2,1,1,1) instant <-  mk_sl_slave_tg(0);
  endmodule

  module mk_sl_slave_tg#(Integer source_id)(Ifc_sl_slave_tg#(a,w,o,i,op,acks,u))
    provisos(Add#(a__, op, 3));

    Reg#(Bool) rg_start <- mkReg(False);
    Reg#(Bool) rg_finish <- mkReg(False);

    // Slave Agent
    Ifc_slc_slave_agent#(a,w,o,i,op,acks,u) sl_slave_agent <- mkslc_slave_agent();

    LFSR#(Bit#(4))  rand_event <- mkLFSR_4();

    //Reg#(Bit#(z)) rg_beat_counter <- mkReg(0);

    FIFO#(Bit#(a)) fifo_txn_signature <- mkSizedFIFO(2);

    Reg#(Bit#(o)) rg_acquire_source <- mkReg(unpack(0));
    Reg#(Bit#(3)) rg_acquire_size    <- mkReg(unpack(0));
    Reg#(Bit#(u)) rg_crit    <- mkReg(unpack(0));
    Reg#(Bit#(3)) rg_acquire_serviced<- mkReg(unpack(0));
    Reg#(Bit#(o)) rg_release_source <- mkReg(unpack(0));
    Reg#(Bit#(3)) rg_release_size    <- mkReg(unpack(0));

    Reg#(Bool) rg_acquire_block <- mkReg(False);
    Reg#(Bool) rg_start_a <- mkReg(False);
		Reg#(Bool) rg_start_d <- mkReg(False);
		Reg#(Bool) rg_start_r <- mkReg(False);
		Reg#(Bool) rg_start_rd <- mkReg(False);
		Reg#(Bool) rg_start_b <- mkReg(False);

    rule rl_reset_acquire_block ( ! rg_acquire_block );
      rg_start_a <= True;
      rg_acquire_block <= True;
    endrule

    rule rl_drive_A (rg_start_a == True);
      Req_channel#(a,w,o,i,op,u) packet_acquire = sl_slave_agent.o_req_channel.first();
      sl_slave_agent.o_req_channel.deq();
      rg_acquire_size   <= packet_acquire.size;
      rg_acquire_source <= packet_acquire.source;
      rg_crit <= packet_acquire.user;
      rg_acquire_serviced <= 0;
      $display($time, "Req recv %d\t\t",source_id,fshow(packet_acquire));
      rg_start_a <= False;
      //rg_start_b <= True;
      rg_start_d <= True;
    endrule

    // Concurrent B - Channel handling
    // B- Channel fwd response
    // B- Channel fwd-grant   response
    rule rl_drive_B(rg_start_b == True);
      // dequeue - look at opcode - update relevant counter
      Fwd_channel#(a,w,o,i,op,u) grant_packet = ?;
      Bit#(op) lv_op = truncate(pack(GrantData));
      grant_packet.opcode = unpack(lv_op);
      grant_packet.user = rg_crit;
      grant_packet.source = rg_acquire_source;
      //grant_packet.size = rg_acquire_size;
      grant_packet.dest = fromInteger(source_id);
      sl_slave_agent.i_fwd_channel.enq(grant_packet);
      if(rg_acquire_serviced < rg_acquire_size)
        rg_acquire_serviced <= rg_acquire_serviced +1;
      else begin
        rg_start_b <= False;
        //rg_acquire_block <= False;
        end
    endrule


    // Concurrent D - Channel handling
    // D- Channel release response
    // D- Channel grant   response
    rule rl_drive_D(rg_start_d == True);
      // dequeue - look at opcode - update relevant counter
      Resp_channel#(a,w,o,i,op,acks,u) grant_packet = ?;
      Bit#(op) lv_op = truncate(pack(GrantData));
      //grant_packet.opcode = unpack(lv_op);
      grant_packet.user = rg_crit;
      $display($time, "Response gen: %d",source_id,fshow(grant_packet));
      grant_packet.source = rg_acquire_source;
      //grant_packet.size = rg_acquire_size;
      grant_packet.dest = fromInteger(source_id);
      sl_slave_agent.i_resp_channel.enq(grant_packet);
      if(rg_acquire_serviced < rg_acquire_size)
        rg_acquire_serviced <= rg_acquire_serviced +1;
      else begin
        rg_start_d <= False;
        rg_acquire_block <= False;
        end
    endrule

    // B- Channel
    // tl_slave_agent.i_b_channel

    // // Start
    Reg#(Bit#(4)) rg_event <- mkReg(0);

    rule rl_activity(rg_start && (!rg_finish));
      rg_event <= rand_event.value();
    //   rand_event.next();
    endrule

    // rule rl_acquire(rg_event[0] == 1'b1);
    //   fsm_grant.start();
    // endrule
    // rule rl_probe_dirty(rg_event[1] == 1'b1);
    //   fsm_probe.start();
    // endrule
    // rule rl_release(rg_event[2] == 1'b1);
    //   fsm_release_ack.start();
    // endrule

    // Finish

    method Action ma_start if(!rg_start);
      rg_start <= True;
      rand_event.seed(4'hf);
    endmethod
    method Action ma_finish if (rg_start);
      rg_finish <= True;
    endmethod
    method Action ma_rand_value(Bit#(a) lfsr_value) if(rg_start);
      fifo_txn_signature.enq(lfsr_value);
    endmethod
    method Bool mv_done if (rg_finish);
      return True;
    endmethod

    interface sl_slave = sl_slave_agent.shaktilink_side;
  endmodule
endpackage :tb_shakti_slave
