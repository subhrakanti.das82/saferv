/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package bram_slc;
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import GetPut :: * ;
  import Connectable :: * ;
  import Vector :: * ;
  import BRAMCore :: * ;

  import coherence_types :: * ;
  import llc_bank ::*;
  import common_types:: * ;
  import Soc_crossbar_types :: * ;
  `include "coherence.defines"
  import ShaktiLink_Types :: * ;
  import ShaktiLink_Fabric :: * ;
  import Semi_FIFOF :: * ;

  interface Ifc_bram_slc#(numeric type a, numeric type w, numeric type o, 
                          numeric type i, numeric type op, numeric type acks, numeric type u);
      interface Ifc_slc_slave#(a,w,o,i,op,acks,u) slave_side;
  endinterface
  module mkbram_slc#(parameter String modulename )(Ifc_bram_slc#(a,w,o,i,op,acks,u))
    provisos(
      Mul#(TDiv#(TMul#(w, 8), TDiv#(TMul#(w, 8), 8)), TDiv#(TMul#(w, 8), 8),TMul#(w, 8)),
      Add#(0, o, i),
      Add#(a__, 3, i),
      Add#(b__, 4, op)
    );
    BramUserInterface#(a, TMul#(w,8), 18) dut <- mkbram(0, "code.mem", modulename);
    Ifc_slc_slave_agent#(a,w,o,i, op,acks,u) slave <- mkslc_slave_agent;

		FIFOF#(Req_channel#(a,w,o,i,op,u)) ff_req <- mkFIFOF();
    rule read_request_first;
		  let req <- pop_o(slave.o_req_channel);
		  Message#(a,w) msg = fn_from_req_pkt(req);
		  if (msg.msgtype == GetS) begin
        dut.read_request(req.address);
        ff_req.enq(req);
        `logLevel( bram, 0, $format(modulename,": Initiating Read Req:",fshow(msg)))
      end
      else begin
        dut.write_request(tuple3(req.address, req.data,'1));
        `logLevel( bram, 0, $format(modulename,": Performing Write:",fshow(msg)))
      end
    endrule
//    // get data from the memory. shift,  truncate, duplicate based on the size and offset.
    rule read_response;
      let {err, data0}<-dut.read_response;
      let req = ff_req.first;
      ff_req.deq;
      Resp_channel#(a,w,o,i,op,acks,u) _r = Resp_channel{ opcode: req.opcode,
                          acksExpected:0,
                          last: True,
                          source: (req.dest), 
                          dest: (req.source), 
                          address:req.address, 
                          corrupt:0,
                          data:data0,
                          tid: req.tid,
                          user: req.user}; 
			slave.i_resp_channel.enq(_r);
			`logLevel( tb, 0, $format(modulename,": Sending resposne:",fshow(_r)))
    endrule
    interface slave_side = slave.shaktilink_side;
  endmodule
  interface BramUserInterface#(numeric type addr_width,  numeric type data_width, numeric type index_size);
    method Action read_request (Bit#(addr_width) addr);
    method Action write_request (Tuple3#(Bit#(addr_width), Bit#(data_width),
                                                                  Bit#(TDiv#(data_width, 8))) req);
    method ActionValue#(Tuple2#(Bool, Bit#(data_width))) read_response;
    method ActionValue#(Bool) write_response;
  endinterface

  // to make is synthesizable replace addr_width with Physical Address width
  // data_width with data lane width
  module mkbram#(parameter Integer slave_base, parameter String readfile,
                                                parameter String modulename )
      (BramUserInterface#(addr_width, data_width, index_size))
      provisos(
        Mul#(TDiv#(data_width, TDiv#(data_width, 8)), TDiv#(data_width, 8),data_width)  );

    Integer byte_offset = valueOf(TLog#(TDiv#(data_width, 8)));
  	// we create 2 32-bit BRAMs since the xilinx tool is easily able to map them to BRAM32BE cells
  	// which makes it easy to use data2mem for updating the bit file.

    BRAM_DUAL_PORT_BE#(Bit#(TSub#(index_size, TLog#(TDiv#(data_width, 8)))), Bit#(data_width),
                                                                    TDiv#(data_width,8)) dmemMSB <-
          mkBRAMCore2BELoad(valueOf(TExp#(TSub#(index_size, TLog#(TDiv#(data_width, 8))))), False,
                            readfile, False);

    Reg#(Bool) read_request_sent[2] <-mkCRegA(2,False);

    // A write request to memory. Single cycle operation.
    // This model assumes that the master sends the data strb aligned for the data_width bytes.
    // Eg. : is size is HWord at address 0x2 then the wstrb for 64-bit data_width is: 'b00001100
    // And the data on the write channel is assumed to be duplicated.
    method Action write_request (Tuple3#(Bit#(addr_width), Bit#(data_width),
                                                                  Bit#(TDiv#(data_width, 8))) req);
      let {addr, data, strb}=req;
			Bit#(TSub#(index_size,TLog#(TDiv#(data_width, 8)))) index_address=
			                          (addr - fromInteger(slave_base))[valueOf(index_size)-1:byte_offset];
			dmemMSB.b.put(strb,index_address,truncateLSB(data));
      `logLevel( bram, 0, $format("",modulename,": Recieved Write Request for Address: %h Index: %h\
 Data: %h wrstrb: %h", addr, index_address, data, strb))
  	endmethod

    // The write response will always be an error.
    method ActionValue#(Bool) write_response;
      return False;
    endmethod

    // capture a read_request and latch the address on a BRAM.
    method Action read_request (Bit#(addr_width) addr);
			Bit#(TSub#(index_size,TLog#(TDiv#(data_width, 8)))) index_address=
			                          (addr - fromInteger(slave_base))[valueOf(index_size)-1:byte_offset];
      dmemMSB.a.put(0, index_address, ?);
      read_request_sent[1]<= True;
      `logLevel( bram, 0, $format("",modulename,": Recieved Read Request for Address: %h Index: %h",
                                                                            addr, index_address))
  	endmethod

    // respond with data from the BRAM.
    method ActionValue#(Tuple2#(Bool, Bit#(data_width))) read_response if(read_request_sent[0]);
      read_request_sent[0]<=False;
      return tuple2(False, dmemMSB.a.read());
    endmethod
  endmodule
endpackage

