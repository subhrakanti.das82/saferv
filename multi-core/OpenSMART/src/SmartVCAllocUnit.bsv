import Vector::*;
import Types::*;
import Fifo::*;
import VCAllocUnitTypes::*;
import MessageTypes::*;
import CreditReg::*;
import VirtualChannelTypes::*;
import CreditTypes::*;
import CReg::*;
import FIFOF::*;


interface SmartVCAllocUnit;
  method Bool isInited;
  method HasVC hasVC;
  //method Bool hasVC2;
  method ActionValue#(VCIdx) getNextVC(MsgType msgType, CritType critType);

  method Action putFreeVC(MsgType msgType, CritType critType, VCIdx vc);
endinterface

(* synthesize *)
module mkSmartVCAllocUnit(SmartVCAllocUnit);
  Reg#(Bool)                 inited     <- mkReg(False);
  Reg#(VCIdx)                initCount  <- mkReg(0);
  Reg#(Bit#(64))              lv_counter <- mkReg(0);
  CReg#(2, CreditSignal) rg_credit <- mkCReg(Invalid);

/// Separate freeVCPool for each criticality level and VNETs
  //Vector#(CritLevels, Vector#(NumVNETs, Fifo#(2, VCIdx)))       freeVCPool <- replicateM(replicateM(mkBypassFifo));
	Vector#(CritLevels, Vector#(NumVNETs, FIFOF#(VCIdx)))       freeVCPool <- replicateM(replicateM(mkGSizedFIFOF(False, True, 2)));
//  CreditReg                  creditReg  <- mkCreditReg;

// freeVCPool stores the id of free VCs. Initially (at reset), all VCs are free.
// So, freeVCPool stores the id of all VCs of a input port.
  rule initialize(!inited);
    if(initCount < fromInteger(valueOf(NumVCs))) begin
      let lv_vnetID = initCount/fromInteger(valueOf(VCperVNET)) ;
      let lv_crit = (initCount)%2;
      freeVCPool[lv_crit][lv_vnetID].enq(initCount);
      //$display("Dividing 3/2 : gives %d ",3/VCperVNET );
      //$dispilay($time, "VNET: %d is having VC: %d ", lv_vnetID, initCount);
      //$display($time, "VCid : %d is of crit: %d and VNET: %d", initCount, lv_crit,lv_vnetID);
      initCount <= initCount+1;
    end
    else begin
      inited <= True;
    end
  endrule

/*
//Rule to sanity check if VCid correctly map to VNETs
  rule whenInited(inited);
      lv_counter <= lv_counter + 1;
      if(lv_counter < 16) begin

        for(Integer i = 0; i < fromInteger(valueOf(NumVCs)); i = i +1) begin
          let lv_vnetID = i/fromInteger(valueOf(VCperVNET)) ;
          $display($time, "VNET: %d is having VC: %d ",lv_vnetID , freeVCPool[lv_vnetID].first);
        end

      end
  endrule
*/

  ///function Bool hasAvailableVC = freeVCPool.notEmpty;
  /// hasAvailable VC for each critiality in each VNET
  function HasVC hasAvailableVC;
    HasVC lv_hv = ?;
    for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit +1) begin
      for(Integer vnetId = 0; vnetId < fromInteger(valueOf(NumVNETs)); vnetId = vnetId+1) begin
        lv_hv[crit][vnetId] =(freeVCPool[crit][vnetId].notEmpty?1:0);
      end
    end
    return lv_hv;
  endfunction

  rule rl_update_freevc(inited && isValid(rg_credit[0]));
    let lv_credit = validValue(rg_credit[0]);
    let lv_vc = lv_credit.vc;
    let lv_crit = lv_credit.critType;
    let lv_vnet = (lv_credit.msgType);
    freeVCPool[lv_crit][lv_vnet].enq(lv_vc);
    rg_credit[0] <= tagged Invalid;
  endrule

  method Bool isInited = inited;
//  method Bool hasVC = freeVCPool.notEmpty;
  method HasVC hasVC = hasAvailableVC;
//  method Bool hasVC2 = hasAvailableVC;

// TODO
// This method is called by DataLink interface.
// When a flit leaves the output port buffer, the new VC where the flit will reside
// is updated by calling this method. The VCAllocUnit dequeues this allocated VC from the
// pool.
// This method is called by someone, who wishes to occupy the VC, and so, the VCid won't be free
  method ActionValue#(VCIdx) getNextVC(MsgType msgType, CritType critType);

    //let lv_msgType = fromInteger(valueOf(msgType));
    //let lv_vnet = getVnetID(msgType);
    let lv_vnet = msgType;
    let lv_crit = critType;
    let vc = freeVCPool[lv_crit][lv_vnet].first;
    freeVCPool[lv_crit][lv_vnet].deq;
    return vc;
  endmethod

  // For a single flit packet, this method should be called when a flit leaves a VC, so
  // increment the count of free VC or store this VCid in the freeVCPool.
  method Action putFreeVC(MsgType msgType, CritType critType, VCIdx vc) if(inited);
    //let lv_msgType = fromInteger(valueOf(msgType));
    //let lv_vnet = getVnetID(msgType);
    //if(freeVCPool[lv_vnet].notFull) begin
      //let lv_vnet = 2;
      rg_credit[1] <= tagged Valid CreditSignal_{msgType:msgType, vc:vc, critType:critType};
      //$display($time, "\tputFreeVC getting called!");
      //freeVCPool[lv_vnet].enq(vc);
    //end
  endmethod
endmodule
