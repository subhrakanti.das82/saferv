import Vector::*;
import Fifo::*;
import DReg::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

interface StatLogger;
  method Action incSendCount(MsgType msg, CritType crit);
  method Action incRecvCount(MsgType msg, CritType crit);
  method Action incLatencyCount(CritType crit, Data latency);
  method Action incHopCount(Data hopCount);
  method Action incInflightLatencyCount(Data latency);

  method Vector#(CritLevels,Vector#(NumVNETs, Data)) getSendCount;
  method Vector#(CritLevels,Vector#(NumVNETs, Data)) getRecvCount;
  method Vector#(CritLevels, Data) getLatencyCount;
  method Data getHopCount;
  method Data getInflightLatencyCount;
endinterface

(* synthesize *)
module mkStatLogger(StatLogger);

  Vector#(CritLevels, Vector#(NumVNETs, Reg#(Data))) send_count <- replicateM(replicateM(mkReg(0)));
  Vector#(CritLevels, Vector#(NumVNETs, Reg#(Data))) recv_count <- replicateM(replicateM(mkReg(0)));
  Vector#(CritLevels,Reg#(Data)) latency_count          <- replicateM(mkReg(0));
  Reg#(Data) hop_count              <- mkReg(0);
  Reg#(Data) inflight_latency_count <- mkReg(0);


  method Action incSendCount(MsgType msg, CritType crit);
		let lv_crit = crit;
    let lv_vnet = (msg);
    send_count[crit][lv_vnet] <= send_count[crit][lv_vnet] + 1;
  endmethod

  method Action incRecvCount(MsgType msg, CritType crit);
		let lv_vnet = (msg);
    let lv_crit = crit;
    recv_count[lv_crit][lv_vnet] <= recv_count[lv_crit][lv_vnet] + 1;
  endmethod

  method Action incLatencyCount(CritType crit, Data latency);
    latency_count[crit] <= latency_count[crit] + latency;
  endmethod

  method Action incHopCount(Data hopCount);
    hop_count <= hop_count + hopCount;
  endmethod

  method Action incInflightLatencyCount(Data latency);
    inflight_latency_count <= inflight_latency_count + latency;
  endmethod

  //function Data f1(Integer i) = send_count[i];
  //interface getSendCount = genWith(f1);

//	function Data f2(Integer i) = recv_count[i];
//	interface getRecvCount = genWith(f2);
  method Vector#(CritLevels,Vector#(NumVNETs, Data)) getSendCount;
			Vector#(CritLevels, Vector#(NumVNETs, (Data))) lv_sendcount = ?;
			for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
        for(Integer vnet = 0; vnet < fromInteger(valueOf(NumVNETs)); vnet = vnet +1)begin
          lv_sendcount[crit][vnet] = send_count[crit][vnet];
        end
      end
     return lv_sendcount;
	endmethod

  method Vector#(CritLevels,Vector#(NumVNETs, Data)) getRecvCount;
			Vector#(CritLevels, Vector#(NumVNETs, (Data))) lv_recvcount = ?;
			for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
        for(Integer vnet = 0; vnet < fromInteger(valueOf(NumVNETs)); vnet = vnet +1)begin
          lv_recvcount[crit][vnet] = recv_count[crit][vnet];
        end
      end
     return lv_recvcount;

	endmethod

  method Vector#(CritLevels, Data) getLatencyCount ;
    Vector#(CritLevels, Data) lv_latency = ?;
     for(Integer crit = 0; crit < fromInteger(valueOf(CritLevels)); crit = crit+1)begin
      lv_latency[crit] = latency_count[crit];
     end
    return lv_latency;
  endmethod

  method Data getHopCount = hop_count;
  method Data getInflightLatencyCount = inflight_latency_count;

	//interface getSendCount = genWith(f1);
	//interface getRecvCount = genWith(f2);

endmodule
