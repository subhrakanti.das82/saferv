import Vector::*;
import Fifo::*;

import Types::*;
import MessageTypes::*;
import VirtualChannelTypes::*;
import RoutingTypes::*;
import CreditTypes::*;

import SwitchAllocTypes::*;
import VCAllocUnitTypes::*;
import RoutingUnitTypes::*;

import InputUnit::*;
import OutputUnit::*;
import CreditUnit::*;
import SwitchAllocUnit::*;
import CrossbarSwitch::*;
import SmartVCAllocUnit::*;

import RoutingUnit::*;

`include "Logger.bsv"


/* Get/Put Context
 *  Think from outside of the module;
 *  Ex) I am "getting" a flit from this module.
 *      I am "putting" a flit toward this module.
 */

typedef struct {
  SARes grantedInPorts;
  SARes grantedOutPorts;
  HeaderBundle hb;
  FlitBundle fb;
} SA2CB deriving (Bits, Eq);

interface DataLink;
  method ActionValue#(Flit)         getFlit;
  method Action                     putFlit(Flit flit);
endinterface

interface ControlLink;
  method ActionValue#(CreditSignal) getCredit;
  method Action                     putCredit(CreditSignal creditSig);
endinterface

interface Router;
  method Bool isInited;
  interface Vector#(NumPorts, DataLink)    dataLinks;
  interface Vector#(NumPorts, ControlLink) controlLinks;
  `ifdef  monitor_link_utilisation
    (* always_ready *)
    method Vector#(NumPorts,Bool) monitor_link_utilisation;
  `endif
endinterface

(* noinline *)
function FlitTypeBundle getFlitTypes(FlitBundle fb);
  FlitTypeBundle ftb = newVector;

  for(Integer inPort = 0;inPort<valueOf(NumPorts);inPort=inPort+1) begin
     ftb[inPort] = isValid(fb[inPort])? Valid(validValue(fb[inPort]).flitType) : Invalid;
  end

  return ftb;
endfunction

//(* noinline *)
function SAReq getSAReq(HeaderBundle hb);
  SAReq saReqBits = newVector;
  Bit#(1) high_part = 0;
  for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort=inPort+1) begin
//ifdef SOURCE_ROUTING
//        saReqBits[inPort] = isValid(hb[inPort])? idx2Dir(validValue(hb[inPort]).routeInfo[0]) : null_;
//endif
      saReqBits[inPort] = isValid(hb[inPort])? validValue(hb[inPort]).routeInfo.nextDir : null_;

  end

  // giving priority to HI over LO
  // finding if there is any HI flit participating in output port arbitration
  for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort=inPort+1) begin
    if(isValid(hb[inPort]))begin
      if(validValue(hb[inPort]).critType==0)begin
        high_part = 1;
      end
    end
  end

  // if HI participating, decline request of LO flits
  for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort=inPort+1) begin
    if(isValid(hb[inPort]))begin
      if(high_part==1 && validValue(hb[inPort]).critType==1)begin
        saReqBits[inPort]=null_;
      end
    end
  end


  return saReqBits;
endfunction

function CritReq getCritReq(HeaderBundle hb);
  CritReq saReqCrit = newVector;
  for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort=inPort+1) begin
    saReqCrit[inPort] = isValid(hb[inPort])? validValue(hb[inPort]).critType : 0;
  end
  return saReqCrit;
endfunction


function VNETreq getVNETreq(HeaderBundle hb);
  VNETreq saReqVNET = newVector;

  for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort=inPort+1) begin
//ifdef SOURCE_ROUTING
//        saReqBits[inPort] = isValid(hb[inPort])? idx2Dir(validValue(hb[inPort]).routeInfo[0]) : null_;
//endif
      saReqVNET[inPort] = isValid(hb[inPort])? validValue(hb[inPort]).msgType : 0;

  end

  return saReqVNET;
endfunction


(* noinline *)
function RouteInfoBundle getRB(HeaderBundle hb, SARes grantedInPorts);

    RouteInfoBundle rb = replicate(Invalid);

    for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort +1)
    begin
      if(isValid(hb[inPort]) && grantedInPorts[inPort] == 1) begin
        let header = validValue(hb[inPort]);
        let nextDirn = getNextDirn(header.routeInfo);
        rb[nextDirn] = Valid(header.routeInfo);
      end
    end
    return rb;

endfunction


(* synthesize *)
module mkBaselineRouter#(parameter Bit#(32) id)(Router);

  /********************************* States *************************************/
  Reg#(Bool)                                   inited        <- mkReg(False);

  //To break rules
  Fifo#(1, FlitBundle)                         flitsBuf      <- mkBypassFifo;
  Fifo#(1, HeaderBundle)                       headersBuf    <- mkBypassFifo;
  Fifo#(1, SARes)                              saResBuf      <- mkBypassFifo;

  //Pipelining
  Fifo#(1, SA2CB)                              sa2cb         <- mkPipelineFifo;

  // Performance monitoring
  Vector#(NumPorts, Wire#(Bool)) vwr_monitor_link_utilisation <- replicateM(mkDWire(False));
  /******************************* Submodules ***********************************/

  /* Input Side */
  Vector#(NumPorts, InputUnit)          inputUnits   <- replicateM(mkInputUnit);
  Vector#(NumPorts, ReverseCreditUnit)  crdUnits     <- replicateM(mkReverseCreditUnit);

  /* In the middle */
  SwitchAllocUnit                       localSAUnit  <- mkSwitchAllocUnit;
  CrossbarSwitch                        cbSwitch     <- mkCrossbarSwitch;

  /* Output Side */
  Vector#(NumPorts, OutputUnit)         outputUnits  <- replicateM(mkOutputUnit);
  Vector#(NumPorts, SmartVCAllocUnit)   vcAllocUnits <- replicateM(mkSmartVCAllocUnit);
  Vector#(NumPorts, RoutingUnit)        routingUnits <- replicateM(mkRoutingUnit);

  /******************************* Functions ***********************************/
  /* Read Inputs */
  function FlitBundle readFlits;
    FlitBundle currentFlits = newVector;

    // for each input Unit, store the winner input port arb flit
    for(Integer inPort=0; inPort<valueOf(NumPorts) ; inPort=inPort+1) begin
      currentFlits[inPort] = inputUnits[inPort].peekFlit;
    end

    return currentFlits;
  endfunction

  function HeaderBundle readHeaders;
    HeaderBundle hb = newVector;

    // for each input Unit, store the winner input port arb header
    for(Integer inPort = 0; inPort<valueOf(NumPorts); inPort = inPort +1) begin
      hb[inPort] =  inputUnits[inPort].peekHeader;
    end

    return hb;
  endfunction

  function Action deqWinnerFlits(SARes saRes, FlitBundle fb);
  action
       //$display($time, "       Value of saRes is : %b", saRes);
    for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort+1) begin
      if(saRes[inPort] == 1) begin
        let currFlit = validValue(fb[inPort]);
        inputUnits[inPort].deqFlit;
        crdUnits[inPort].putCredit(Valid(CreditSignal_{vc: currFlit.vc, isTailFlit:True, msgType:currFlit.msgType, critType: currFlit.critType}));
        //$display($time, "\tdeqWinnerFlits calling the crdUnits by putCredit");

        //`logLevel(baseline, 0, $format("deqWinnerFlit getting called for flit",fshow(currFlit)))
      end
    end
  endaction
  endfunction

  function Action putFlit2XBar(SARes saRes, FlitBundle fb, HeaderBundle hb);
  action

    for(Integer inPort = 0; inPort < valueOf(NumPorts); inPort = inPort+1) begin
      if(saRes[inPort] == 1) begin
        let currFlit = validValue(fb[inPort]);
        let destDirn = dir2Idx(validValue(hb[inPort]).routeInfo.nextDir);
        cbSwitch.crossbarPorts[inPort].putFlit(currFlit, destDirn);
      end
    end

  endaction
  endfunction

  function FreeVCInfo getFreeVCInfo;
    FreeVCInfo ret = ?;

    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
      ret[outPort] = vcAllocUnits[outPort].hasVC;
    end

    return ret;
  endfunction

  /****************************** Router Behavior ******************************/
  rule doInitialize(!inited);
    Bit#(1) saInited = localSAUnit.isInited? 1:0;
    Bit#(1) iuInited = 1;
    Bit#(1) vaInited = 1;

    for(DirIdx dirn=0; dirn<fromInteger(valueOf(NumPorts)); dirn=dirn+1) begin
      iuInited = (iuInited == 1  && inputUnits[dirn].isInited)? 1:0;
      vaInited = (vaInited == 1 && vcAllocUnits[dirn].isInited)? 1:0;
    end

    if(saInited==1 && iuInited==1 && vaInited==1) begin
      inited <= True;
    end
  endrule

  // Critical Path Analysis
  // BR -> SA -> SSR Send -> SSR Receive -> SSR Setup
//  (* descending_urgency = "rl_ReqLocalSA, rl_GetLocalSARes, rl_PrepareLocalFlits, rl_deqBufs" *)
  /*********** SA-L ***********/
  rule rl_ReqLocalSA(inited);
    //Read necessary data
    let flits        = readFlits();
    let headers      = readHeaders();
    let saReq        = getSAReq(headers);
    let saVNET       = getVNETreq(headers);
    let saCrit       = getCritReq(headers);
    //$display($time, "     Calling getSAReq.. who all input ports are participating? : ", saReq);
    let freeVCInfo   = getFreeVCInfo;

    // Added display statement to check the availability of VNET at each output port
    for(Integer outPort = 0; outPort < valueOf(NumPorts); outPort = outPort+1) begin
      //$display($time, "     outPort : %d has VNET: %b status", outPort, freeVCInfo[outPort]);
    end

    //$display($time, "Rule1 Gettting fired!");
    //`logLevel(baseline, 0, $format("[%2d]rl_ReqLocalSA",id,fshow(flits)))
    //$display("%10d",$time, "\t Flits buffer contains : ",fshow(flits));
    flitsBuf.enq(flits);
    headersBuf.enq(headers);
    //TODO : freeVCinfo is checked at the output port arbitration.
    // if it is checked at the input port arb, then the winner flit of inpuut port arb
    // has more chance to win in the output port
    // For ex : if a winner flit of input port arb chooses to go for N ouptput port,
    // but gets to know that N output port is not available at output port arb.
    // So, this flit(input port) doens't make progress.
    // Instead,
    // if a flit whose output port is availabe is chosen at the input port arb,
    // then the progress of input port wil be better.
    localSAUnit.reqSA(saReq, freeVCInfo, saVNET, saCrit);
    //`logLevel(baseline, 0, $format("[%2d]rl_ReqLocalSA",id))
  endrule

  rule rl_GetLocalSARes(inited);
    let grantedInPorts  <- localSAUnit.getGrantedInPorts;
    saResBuf.enq(grantedInPorts);
    //$display($time, "   Rule2 : getting arbitration results out Getting Fired!");
    //`logLevel(baseline, 0, $format("[%2d]rl_GetLocalSARes",id))
  endrule


  rule rl_deqFlits(inited);

    //$display($time, "Rule3 : Dequeue Flits, Getting Fired!");
    //`logLevel(baseline, 0, $format("[%2d]rl_deqFlits",id))
    let flits = flitsBuf.first;
    let headers = headersBuf.first;
    let grantedInPorts = saResBuf.first;

    deqWinnerFlits(grantedInPorts, flits);
//
//     //2 stage
// //    putFlit2XBar(grantedInPorts, flits, headers);
//     //3 stage
     //$display($time, "    In rule3 : The grantedInPorts are:  %b", grantedInPorts);
     sa2cb.enq(SA2CB{grantedInPorts: grantedInPorts,
                     grantedOutPorts: ?,
                     hb: headers,
                     fb: flits});

   endrule
   /****************************/
   //For 3-stage
   rule rl_PrepareLocalFlits(inited);
     let x = sa2cb.first;
     let flits           = x.fb;
     let headers         = x.hb;
     let grantedInPorts  = x.grantedInPorts;
    sa2cb.deq;

//   `logLevel(baseline, 0, $format("[%2d]rl_PrepareLocalFlits",id))
    putFlit2XBar(grantedInPorts, flits, headers);
  endrule

  /***********************************/

  /*********** Deque temporary Buffers ***********/

  rule rl_deqBufs(inited);
    //$display("Rule deqBufs getting fired!");
    flitsBuf.deq;
    //`logLevel(baseline, 0, $format("[%2d]rl_deqBufs",id))
    headersBuf.deq;
    saResBuf.deq;
  endrule

  /**********************************************/


  for(Integer prt=0; prt<valueOf(NumPorts); prt = prt+1)
  begin
    rule rl_enqOutBufs(inited);
      let flit <- cbSwitch.crossbarPorts[prt].getFlit;
      //logLevel(baseline, 0, $format("[%2d]rl_enqOutBufs happening at port : %d",id, prt))
      //let newVC <- vcAllocUnits[prt].getNextVC(flit.msgType, flit.critType);
      //flit.vc = newVC;
      outputUnits[prt].putFlit(flit);

    endrule
  end


  /***************************** Router Interface ******************************/

  /* SubInterface routerLinks
   *  => It parameterizes the number of Flit/Credit Links.
   */
  Vector#(NumPorts, DataLink) dataLinksDummy;
  for(DirIdx prt = 0; prt < fromInteger(valueOf(NumPorts)); prt = prt+1)
  begin
      dataLinksDummy[prt] =

        interface DataLink
          method ActionValue#(Flit) getFlit;
            let retFlit <- outputUnits[prt].getFlit;
//            let retFlit <- cbSwitch.crossbarPorts[prt].getFlit;
///TODO:
///During Multi-Flit implementation :
///(1) vc will be stored only in the header flit
///(2) MsgType will be stored only in the header flit
/// Maybe VCTable can help !!

            //Update VC

            let newVC <- vcAllocUnits[prt].getNextVC(retFlit.msgType, retFlit.critType);
            retFlit.vc = newVC;

            //Update Routing Information
//            retFlit.routeInfo = nextRoutingInfo(retFlit.routeInfo); // Source Routing
            retFlit.routeInfo = routingUnits[prt].smartOutportCompute(retFlit.routeInfo, prt);

            `ifdef DETAILED_STATISTICS
            retFlit.stat.hopCount = retFlit.stat.hopCount + 1;
            `endif
            return retFlit;
	  endmethod

          method Action putFlit(Flit flit);
            inputUnits[prt].putFlit(flit);
            vwr_monitor_link_utilisation[prt] <= True; // indicate link utilisation
          endmethod

        endinterface;
  end

  Vector#(NumPorts, ControlLink) controlLinksDummy;
  for(DirIdx prt = 0; prt < fromInteger(valueOf(NumPorts)); prt = prt+1)
  begin
    controlLinksDummy[prt] =
      interface ControlLink
        method ActionValue#(CreditSignal) getCredit if(inited);
          let credit <- crdUnits[prt].getCredit();
          return credit;
        endmethod

        method Action putCredit(CreditSignal creditSig) if(inited);
          if(isValid(creditSig)) begin
            	vcAllocUnits[prt].putFreeVC(validValue(creditSig).msgType, validValue(creditSig).critType, validValue(creditSig).vc);
          end
        endmethod

      endinterface;
  end

  interface dataLinks = dataLinksDummy;
  interface controlLinks = controlLinksDummy;

  method Bool isInited;
    return inited;
  endmethod

  `ifdef  monitor_link_utilisation
    method Vector#(NumPorts,Bool) monitor_link_utilisation;
      Vector#(NumPorts,Bool) lv_ret_val = replicate(False);
      for(Integer prt = 0; prt < valueOf(NumPorts); prt = prt+1) begin
        lv_ret_val[prt] = vwr_monitor_link_utilisation[prt];
      end
      return lv_ret_val;
    endmethod
  `endif

endmodule
