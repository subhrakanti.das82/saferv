## Static Mapping of criticality (HI/LO) to each VC and stress testing network with ShaktiLink NIC


### Network Configuration:
4*4 mesh, XY routing, simulation time = 150,000 cycles, vc_per_vnet = 2(HI + LO), #vnet = 4, wormhole flow control

### Commands used:  
Here, pwd(present working directory) is OpenSMART

1. Clone fabrics directory: 
```bash
$ ./manager.sh update_deps
```
2. Make: 
```bash
$ make generate_verilog link_verilator simulate > log
```
3. Generate histogram : 
```bash
$ python3 hist_vcs.py
```

### Some Essentials and Proposed Method: 
* The network injector can inject high critical(HI)/low critical(LO) packets into the network which is controlled in testbench: fabrics/shaktilink/tb_shakti_master.bsv 

* A HI critical packet dominates over LO critical packet. This is implemented in input and output port arbitration: 
    * During input port arbitration at one input port, if there is any HI critical flit participating then, no LO critical flit is allowed to participate.
    * During output port arbitration at one output port, if there is any HI critical flit participating then, no LO critical flit is allowed to participate for that output port.

* If hi_per_vnet is 1 then, a the VCid of a flit always remains the same as the flit is allowed to travese in the same vnet and same criticality.

* For ex: For this scheme, the input port looks like: 
* | VCid | VNET| Criticality |
  | ---  |---  |  ------  |
  | VC0  | VNET0 | HI   |
  | VC1  | VNET0 | LO   |
  | VC2  | VNET1 | HI   |
  | VC3  | VNET1 | LO   |
  | VC4  | VNET2 | HI   |
  | VC5  | VNET2 | LO   |
  | VC6  | VNET3 | HI   |
  | VC7  | VNET3 | LO   |
* VNET0 carries Req messages
* VNET1 carries Fwd messages
* VNET2 carries Resp messages
* VNET3 carries System monitoring messages

### Observations and Results:

* Intuitively, the latency characteristics of HI packets should improve as they can preempt LO, as they face less contention compared to baseline model. Comparing baseline and case1 matches the intuition. 

* A short example : 
    * Average and Tail packet latency comparison: 
    * Case1: Baseline
    * |  | max | average |no. of packets|
         | ---  |  ------  |----------|----------|
         | Baseline| 594   | 14.22   | 99852|
    * NOTE: Baseline is the case when criticality is disabled

    * Case2: 25% HI injected, 75% LO injected
    * |  | max | average |no. of packets|
         | ---  |  ------  |----------|----------|
         | HI| 300   | 13.10   | 24954|
         | LO| 554   | 14.36   | 74784|
         
    * Case3: 50% HI injected, 50% LO injected
    * |  | max | average |no. of packets|
         | ---  |  ------  |----------|----------|
         | HI| 587   | 13.53   | 49875|
         | LO| 603   | 14.85   | 49851|
* Note: as of now percentage of HI/LO is not parameterized in the testbench(WIP)
    
* Plots of histogram(frequency of packets vs latency)(attached) to compare baseline and Case2:
    * Baseline: ![Figure1](./pictures/baseline.jpg "Figure1 : baseline")
    * Case1: ![Figure2](./pictures/case1.jpg "Figure2 : case1")

* A significant improvement in upper bound of latency is observed from baseline to case1 for HI: (594 -> 300)
* An improvement in average latency is also observed for HI: 14.21 -> 13.10
* The network injects req packets whenever credit is available. 2 Resp packets are generated for any ejected Req packet. For this experiment, till 100k cycles req flits are allowed to be injected. The simulation ends at 150k cycles which gives a 50k cycle gap for remaining flits to reach their destination.



