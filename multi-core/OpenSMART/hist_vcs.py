# to plot historgram for scheme2 : like combine VC8,9


#!/usr/bin/env python
# get file1
#fname1 = input("> Which file? ")
#inj_rate1 = input("> Corresponding injection rate? ")
fname1 = "dump_file2.dat"
inj_rate1 = "0.24"
with open(fname1) as f:
  content1 = f.readlines()

content1 = [x.strip() for x in content1]
lis1 = []
for x in content1:
  lis1.append(int(x))


#print (lis1)

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


#plotting graph1
minimum = min(lis1)
maximum = max(lis1)
print("Total packets: " + str(len(lis1)))
print("The max latency is : " + str(maximum) + " Cycles")
print("The min latency is : " + str(minimum) + " Cycles")
print("The avg latency is : " + str(sum(lis1)/len(lis1)) +  " Cycles")
legend = ['injection rate = maximum']
bins = maximum - minimum
#plt.subplot(221) # the first figure
plt.hist(lis1,bins)
plt.xlabel("Latency ")
plt.ylabel("Number of packets")
plt.title("LO_crit_packets")
plt.legend(legend)
#plt.show()

#plt.show()
# save the figure
plt.savefig('LO_crit.jpg', format='jpg')

