/* 
Copyright (c) 2019, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
  with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: Neel Gala
Email id: neelgala@gmail.com
Details:

--------------------------------------------------------------------------------------------------
*/
package TbSoc_crossbar;
  `include "Logger.bsv"
  import FIFO :: * ;
  import FIFOF :: * ;
  import SpecialFIFOs :: * ;
  import DReg :: * ;
  import GetPut :: * ;

  import Soc_crossbar_types ::*;
  import Soc_crossbar :: * ;
  import RS232_modified :: *;
  import uart :: *;
  import Vector :: * ;
  import common_types :: * ;
  import device_common :: * ;

  /*doc:module: */
  module mkTbSoc_crossbar (Empty);
    Ifc_Soc_crossbar soc <- mk_Soc_crossbar;
    UserInterface#(`paddr,`vaddr,48) uart <- mkuart_user(2, pack(STOP_1), pack(ODD));
    Reg#(Bit#(5)) rg_cnt <-mkReg(0);
    Reg#(Bool) rg_read_rx<- mkDReg(False);
    Vector#(`NrCaches,Wire#(Maybe#(DumpType))) wr_io_dump <- replicateM(mkDWire(tagged Invalid));

    rule display_eol;
	    let timeval <- $time;
      `logLevel( tb, 0, $format("\n[%10d]", timeval))
    endrule
  `ifdef rtldump
 	  let dump <- mkReg(InvalidFile) ;
    rule open_file_rtldump(rg_cnt<1);
      let generate_dump <- $test$plusargs("rtldump");
      if(generate_dump) begin
        String dumpFile = "rtl.dump" ;
    	  File lfh <- $fopen( dumpFile, "w" ) ;
    	  if ( lfh == InvalidFile )begin
    	    `logLevel( tb, 0, $format("TB: cannot open %s", dumpFile))
    	    $finish(0);
    	  end
    	  dump <= lfh ;
      end
    endrule
  `endif
 	  let dump1 <- mkReg(InvalidFile) ;
    rule open_file_app(rg_cnt<1);
      String dumpFile1 = "app_log" ;
    	File lfh1 <- $fopen( dumpFile1, "w" ) ;
    	if (lfh1==InvalidFile )begin
    	  `logLevel( tb, 0, $format("TB: cannot open %s", dumpFile1))
    	  $finish(0);
    	end
      dump1 <= lfh1;
    	rg_cnt <= rg_cnt+1 ;
    endrule
    rule connect_uart_out;
      soc.uart_io.sin(uart.io.sout);
    endrule
    rule connect_uart_in;
      uart.io.sin(soc.uart_io.sout);
    endrule
    rule check_if_character_present(!rg_read_rx);
      let {data,err}<- uart.read_req('hc,Byte);
      if (data[3]==1) // character present
        rg_read_rx<=True;
    endrule
    rule write_received_character(rg_cnt>=1 && rg_read_rx);
      let {data,err}<-uart.read_req('h8,Byte);
      $fwrite(dump1,"%c",data);
    endrule
  `ifdef rtldump
    for (Integer i = 0; i<`NrCaches; i = i + 1) begin
      /*doc:rule: */
      rule rl_capture_io_dump;
        let x <- soc.io_dump[i].get;
        wr_io_dump[i] <= tagged Valid x;
      endrule  
    end
    rule write_dump_file(rg_cnt>=1);
      let generate_dump <- $test$plusargs("rtldump");
      for (Integer i = 0; i<`NrCaches; i = i + 1) begin
        if(wr_io_dump[i] matches tagged Valid .d) begin
          let {prv, pc, instruction, rd, data, rdtype} = d;
        `ifndef openocd
          if(instruction=='h00006f||instruction =='h00a001)
            $finish(0);
          else
        `endif
          if(generate_dump)begin
          	$fwrite(dump, "%2d ", i, prv, " 0x%16h", pc, " (0x%8h", instruction, ")");
            if(rdtype == FRF && valueOf(FLEN) == 64)
          	  $fwrite(dump, " f%d", rd, " 0x%16h", data[63:0], "\n");
            else if(rdtype == FRF && valueOf(FLEN) == 32)
          	  $fwrite(dump, " f%d", rd, " 0x%8h", data[31:0], "\n");
            else if(rdtype == IRF && valueOf(XLEN) == 64)
        	    $fwrite(dump, " x%d", rd, " 0x%16h", data[63:0], "\n");
            else if(rdtype == IRF && valueOf(XLEN) == 32)
        	    $fwrite(dump, " x%d", rd, " 0x%8h", data[31:0], "\n");
          end
        end
      end
    endrule
  `endif

  endmodule
endpackage

