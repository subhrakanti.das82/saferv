# ProtoBSV

This is an extension of the ProtoGen framework available here: https://github.com/icsa-caps/ProtoGen

This work adds a BSV backend to ProtoGen

# Installation and Pre-requisites

1. Install python-3.7
2. pip install colorama tabulate seqdiag
3. Install antlr3 from here: https://github.com/antlr/antlr3/tree/master/runtime/Python3

# Usage
```
python ProtoGen.py -i MSI_Proto.pcc -b bsv --verbose debug
```


